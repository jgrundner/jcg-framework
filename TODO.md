# JCG Application TODOs

## Application

- Integrate Knp menu plugin with bootstrap css
- Caching
    - Try: https://github.com/KuiKui/MemcacheServiceProvider
- Annotations for routing
    - Find a library that works correctly with Silex 2
    - https://github.com/eltrino/PHPUnit_MockAnnotations 
- Rework all configs to yaml?
    - Unsure on this because it causes overhead of Yaml to Array conversion.
- Console
    - Add functionality as needed

## Write Tests

- Test all the things

---


Notes
---
###### To fix ssh error not finding key:
```
ssh-add ~/.ssh/jgrundner_bitbucket
```

###### Password: foo
*sha512:* 5FZ2Z8QIkA7UTZ4BYkoC+GsReLf569mSKDsfods6LYQ8t+a8EW9oaircfMpmaLbPBh4FOBiiFyLfuZmTSUwzZg==
*BCrypt:* $2y$04$bZuC8L4I4IkFRIUoZcYUHO0QbQYYun8/1JWontvwoBM1ULWjJbksy
