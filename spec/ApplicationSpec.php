<?php


namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ApplicationSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Application');
    }

    function it_gets_root_directory()
    {
        $this->getRootDir()->shouldReturn(dirname(dirname(__FILE__)));
    }

    function it_should_passthrough_share_parameter()
    {
        $testFunction = function(){
            return true;
        };
        $this->share($testFunction)->shouldReturn($testFunction);
    }

}
