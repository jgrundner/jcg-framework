<?php


namespace spec\App\Provider;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Prophecy\Prophet;
use Doctrine\DBAL\Connection;
use Symfony\Component\Security\Core\User\User;
use App\Entity\Auth\User as JcgUser;
use spec\App\Provider\TestUser;

class UserProviderSpec extends ObjectBehavior
{
    protected $connection;

    /**
     * @param \Doctrine\DBAL\Connection $connection
     */
    function let(Connection $connection)
    {
        $prophet = new Prophet;
        $this->connection = $prophet->prophesize('Doctrine\DBAL\Connection');
        $this->beConstructedWith($this->connection);
    }

    function it_should_check_for_user_class()
    {
        $class = 'Symfony\Component\Security\Core\User\User';
        $this->supportsClass($class)->shouldBe(true);
    }

    function it_should_fail_for_wrong_class()
    {
        $class = 'Symfony\Component\Security\Core\User\UserFake';
        $this->supportsClass($class)->shouldBe(false);
    }

    function it_should_return_user()
    {
        // Set up data
        $query = "SELECT * FROM users WHERE username = ?";
        $username = 'admin@jgrundner.com';
        $user = [ 'username'=> $username, 'password_crypt'=>'', 'roles'=>''];

        // Fake query statement build
        $prophet = new Prophet;
        $stmt = $prophet->prophesize('Doctrine\DBAL\Driver\Statement');
        $this->connection->executeQuery($query, ['admin@jgrundner.com'] )->willReturn($stmt);

        // Fake statement execute
        $stmt->fetch()->willReturn($user);

        // Call function test
        $this->loadUserByUsername($username)->shouldBeAnInstanceOf('Symfony\Component\Security\Core\User\User');
    }

//    function it_should_refresh_user()
//    {
//        $username = 'admin@jgrundner.com';
//
//        $prophet = new Prophet;
//        $user = $prophet->prophesize('spec\App\Provider\TestUser');
//        $user->willImplement('Symfony\Component\Security\Core\User\UserInterface');
//        $user->getUsername()->willReturn($username);
//        $stub = $user->reveal();
//
//        $this->refreshUser($stub)->shouldMatch($username);
//
//    }
}
