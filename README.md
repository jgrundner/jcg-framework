JCG Framework Implementation
==============

A fully-functional Silex application that you can use as the skeleton for your new applications. It is based on Silex @2.0-dev and Symfony ~2.7 components. The use of Silex dev may cause some bug as it is not yet in a stable release but I wanted to get a jump start on the latest changes to the core.

This release is great for rapid prototyping as it comes with a built in Vagrant virtual machine. This machine image is running PHP 7.0.0-dev FPM through Nginx on a Ubuntu 14.04-x64 test box. I have made modifications to it, including sharing the application's root directory.

This document contains information on how to get started.

Creating the Application
----------------------------

This is assuming that you have [Composer], sqlite3, and vagrant installed

console:

    $ git clone https://jimigrunge@bitbucket.org/jimigrunge/jcg-framework.git project-name
    $ cd project-name
    $ composer install
    $ sqlite3 app.db < config/sql/001-createDatabase.sql

If you want to run the [Vagrant] VM for [VirtualBox]. ( _I suggest using version 4 of VirtualBox due to existing issues with NFS_ ):

    $ cd vagrant
    $ vagrant up

Browsing the Application
-----------------------------

Congratulations! You're now ready to use the application.

Vagrant virtual machine allows you to run the application locally
You can browse to http://192.168.8.8 to see the application

If you don't have or don't want to use Vagrant you can use the PHP internal dev server

console:

    $ cd path/to/install
    $ composer run

Then, browse to http://localhost:8888/.

Login is:

    un: admin
    pw: foo

Getting started with Silex
--------------------------

This distribution is meant to be the starting point for your applications.

It is built on Silex and Symfony components

A great way to start is via the [Documentation], which will
take you through all the features of Silex.

What's inside?
---------------

The Silex framework is configured with the following service providers already pre installed:

* [UrlGeneratorServiceProvider] - Provides a service for generating URLs for
  named routes.

* [ValidatorServiceProvider] - Provides a service for validating data. It is
  most useful when used with the FormServiceProvider, but can also be used
  standalone.

* [ServiceControllerServiceProvider] - As your Silex application grows, you
  may wish to begin organizing your controllers in a more formal fashion.
  Silex can use controller classes out of the box, but with a bit of work,
  your controllers can be created as services, giving you the full power of
  dependency injection and lazy loading.

* [TwigServiceProvider] - Provides integration with the Twig template engine.

* [WebProfilerServiceProvider] - Enable the Symfony web debug toolbar and
  the Symfony profiler in your Silex application when developing.

* [MonologServiceProvider] - Enable logging in the development environment.

Read the [Providers] documentation for more details about Silex Service
Providers.

Enjoy!

[Composer]: http://getcomposer.org/
[Vagrant]: http://vagrantup.com
[VirtualBox]: https://www.virtualbox.org
[Documentation]: http://silex.sensiolabs.org/documentation
[UrlGeneratorServiceProvider]: http://silex.sensiolabs.org/doc/providers/url_generator.html
[ValidatorServiceProvider]: http://silex.sensiolabs.org/doc/providers/validator.html
[ServiceControllerServiceProvider]: http://silex.sensiolabs.org/doc/providers/service_controller.html
[TwigServiceProvider]: http://silex.sensiolabs.org/doc/providers/twig.html
[WebProfilerServiceProvider]: http://github.com/silexphp/Silex-WebProfiler
[MonologServiceProvider]: http://silex.sensiolabs.org/doc/providers/monolog.html
[Providers]: http://silex.sensiolabs.org/doc/providers.html


