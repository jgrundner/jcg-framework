<?php

namespace App\Loader;

use Application;

/**
 * Class RouteLoader
 * @package App\ApplicationLoaders
 */
class RouteLoader
{
    /**
     *
     */
    public function __construct()
    {

    }

    /**
     * @param Application $app
     * @return Application
     */
    public function loadRoutes(Application $app)
    {
        $directory = $app["routers.dirname"];
        foreach (array_diff(scandir($directory), ['..', '.']) as $filename) {
            $path = $directory . '/' . $filename;
            if (!is_dir($path)){
                require($path);
            }
        }
        return $app;
    }
}
