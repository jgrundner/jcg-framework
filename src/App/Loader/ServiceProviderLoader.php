<?php
namespace App\Loader;

use Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\RoutingServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use \WhoopsPimple\WhoopsServiceProvider;
use Doctrine\Common\Cache\FilesystemCache;
use App\Provider\UserProvider;

/**
 * Class ServiceProviderLoader
 * @package App\Application
 */
class ServiceProviderLoader
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        // Nothing to do here
    }

    /**
     * @param Application $app
     * @return Application
     */
    public function loadServiceProviders(Application $app)
    {
        $app->register(new RoutingServiceProvider());
        $app->register(new ValidatorServiceProvider());
        $app->register(new ServiceControllerServiceProvider());
        $app->register(new HttpFragmentServiceProvider());
        $app->register(new SessionServiceProvider());
        $app->register(new DoctrineServiceProvider());
        $app->register(new DoctrineOrmServiceProvider());
        $app->register(new WhoopsServiceProvider());

//        $whoops = new \Whoops\Run;
//        $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
//        $whoops->register();

        $app = $this->loadTemplatingProvider($app);
        $app = $this->loadSecurityProvider($app);
        //$app = $this->loadAnnotations($app);

        $app = (!empty($app['monolog.logfile']))?$this->loadLogger($app):$app;
        $app = ($app['debug'])?$this->loadWebProfiler($app):$app;

        return $app;
    }

    /**
     * @param Application $app
     * @return Application
     */
    protected function loadTemplatingProvider(Application $app)
    {
        $app->register(new TwigServiceProvider()
            , ['twig.path' => $app["twig.path"],]
        );
        $app['twig'] = $app->extend('twig', function ($twig, $app) {
            $twig->addFunction(new \Twig_SimpleFunction('asset', function ($asset) use ($app) {
                return $app['request_stack']->getMasterRequest()->getBasepath() . '/' . ltrim($asset, '/');
            }));
            return $twig;
        });
        return $app;
    }

    /**
     * @param Application $app
     * @return Application
     */
    protected function loadSecurityProvider(Application $app)
    {
        $app->register(new SecurityServiceProvider(), [
            'security.firewalls' => [
                'login' => [
                    'pattern' => $app['firewall']['login.pattern'],
                    'anonymous' => $app['firewall']['login.anonymous'],
                ],
                'admin' => [
                    'pattern' => $app['firewall']['admin.pattern'],
                    'http' => $app['firewall']['admin.http'],
                    'anonymous' => $app['firewall']['admin.anonymous'],
                    'form' => [
                        'login_path' => $app['firewall']['admin.form']['login_path'],
                        'check_path' => $app['firewall']['admin.form']['check_path'],
                        'default_target_path' => $app['firewall']['admin.form']['default_target_path'],
                    ],
                    'logout' => [
                        'logout_path' => $app['firewall']['admin.logout_path']
                    ],
                    'users' => function ($app) {
                        return new UserProvider($app['db']);
                    },
                ],
                'anon' => [
                    'pattern' => $app['firewall']['anon.pattern'],
                    'anonymous' => $app['firewall']['anon.anonymous'],
                ],
            ],
        ]);
        return $app;
    }

//    /**
//     * @param Application $app
//     * @return Application
//     */
//    protected function loadRouterProvider(Application $app)
//    {
//        $app->register(new RoutingServiceProvider());
//        $router = new SilexRouter($app);
//        $app['routers']->add($router);
//        $app->register(new UrlGeneratorServiceProvider());
//        return $app;
//    }

//    /**
//     * @param Application $app
//     * @return Application
//     */
//    protected function loadAnnotations(Application $app)
//    {
//        $app->register(new OrlexServiceProvider(), [
//            "annot.cache" => new FilesystemCache($app['annotation.cache_dir']),
//            "annot.controllerDir" => $app->getRootDir()."/src/App/Controller",
//            "annot.controllerNamespace" => "App\\Controller\\"
//        ]);
//        return $app;
//    }

    /**
     * @param Application $app
     * @return Application
     */
    protected function loadLogger(Application $app)
    {
        $app->register(new MonologServiceProvider(), [
            'monolog.name' => $app['app.name'],
            'monolog.level' => $app['log']['level'],
            'monolog.logfile' => $app['monolog.logfile'],
        ]);
        return $app;
    }

    /**
     * @param Application $app
     * @return Application
     */
    protected function loadWebProfiler(Application $app)
    {
        $app->register($p = new WebProfilerServiceProvider(), [
            'profiler.cache_dir' => $app['profiler.cache_dir'],
        ]);
        return $app;
    }

}
