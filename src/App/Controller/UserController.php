<?php

namespace App\Controller;

use Symfony\Component\Form\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use \Twig_Environment as Template;
use Symfony\Component\Routing\Generator\UrlGenerator;
use App\Service\RedirectResponseService;
use App\Repository\UserRepository;
use App\Entity\Auth\User;
use \Exception as Exception;


/**
 * Class UserController
 *
 * @Route(path="/")
 *
 * @package App\Controllers
 */
class UserController
{
    /** @var  Template */
    private $template;
    /** @var  UrlGenerator */
    private $urlGenerator;
    /** @var  RedirectResponseService */
    private $redirectService;
    /** @var  UserRepository */
    private $userRepository;

    /**
     * UserController constructor.
     * @param Template $template
     * @param UrlGenerator $urlGenerator
     * @param RedirectResponseService $redirectService
     * @param UserRepository $userRepository
     */
    public function __construct(
        Template                     $template,
        UrlGenerator                 $urlGenerator,
        RedirectResponseService      $redirectService,
        UserRepository               $userRepository
    ) {
        $this->template        = $template;
        $this->urlGenerator    = $urlGenerator;
        $this->redirectService = $redirectService;
        $this->userRepository  = $userRepository;
    }

    /**
     * @Route(path="/user", name="users")
     *
     * @return mixed
     */
    public function indexAction()
    {
        //throw new RuntimeException('hello');
        return $this->template->render('user/list.html.twig', [
            'users' => $this->userRepository->findAll()
        ]);
    }

    /**
     * @Route(path="/user/edit/{userId}", name="user_edit")
     *
     * @param $userId
     * @return mixed
     */
    public function editAction($userId)
    {
        return $this->template->render('user/edit.html.twig', [
            'user' => $this->userRepository->find($userId)
        ]);
    }

    /**
     * @Route(path="/user/save/{userId}", name="user_save", methods={"POST"})
     *
     * @param Request $request
     * @param $userId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveAction(Request $request, $userId)
    {
        $user = $this->userRepository->find($userId);
        $this->userRepository->updateUser($user, $request->request->all());
        return $this->redirectService->redirect($this->urlGenerator->generate('users'), 302);
    }

    /**
     * @Route(path="/user/add", name="user_add")
     *
     * @return mixed
     */
    public function addAction()
    {
        return $this->template->render('user/add.html.twig', []);
    }

    /**
     * @Route(path="/user/create", name="user_create", methods={"POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $user = new User();
        $user = $this->userRepository->addUser($user, $request->request->all());
        return $this->redirectService->redirect($this->urlGenerator->generate('user_edit', ['userId' => $user->getId()]), 302);
    }

    /**
     * @Route(path="/user/delete/{userId}", name="user_delete")
     *
     * @param $userId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($userId)
    {
        $user = $this->userRepository->find($userId);
        $user->delete();
        $this->userRepository->saveUser($user);

        return $this->redirectService->redirect($this->urlGenerator->generate('users'), 302);
    }
}
