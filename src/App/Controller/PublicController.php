<?php

namespace App\Controller;

use Silex\Application as Container;
use Symfony\Component\HttpFoundation\Request;
use \Twig_Environment as Template;
use \Exception as Exception;


/**
 * Class PublicController
 *
 * @Route(path="/")
 *
 * @package App\Controllers
 */
class PublicController
{
    /** @var  Template */
    private $template;

    /**
     * @param Template $template
     */
    public function __construct(Template $template)
    {
        $this->template = $template;
    }

    /**
     * @Route(path="/", name="homepage")
     *
     * @return mixed
     */
    public function indexAction()
    {
        return $this->template->render('index.html.twig', []);
    }

    /**
     * @Route(path="/login", name="login")
     *
     * @param Request $request
     * @param Container $container
     * @return mixed
     */
    public function loginAction(Request $request, Container $container)
    {
        return $this->template->render('login.html', [
            'error'         => $container['security.last_error']($request),
            'last_username' => $container['session']->get('_security.last_username'),
        ]);
    }
}
