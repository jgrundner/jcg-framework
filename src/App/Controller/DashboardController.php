<?php

namespace App\Controller;

use \Twig_Environment as Template;
use \Exception as Exception;


/**
 * Class DashboardController
 *
 * @Route(path="/")
 *
 * @package App\Controllers
 */
class DashboardController
{
    /** @var  Template */
    private $template;

    protected $entityManager;

    /**
     * @param Template $template
     */
    public function __construct(Template $template)
    {
        $this->template = $template;
    }

    /**
     * @Route(path="/dashboard", name="dashboard")
     *
     * @return mixed
     */
    public function indexAction()
    {
        return $this->template->render('dashboard.html.twig', [
            /* nothing to do yet */
        ]);
    }
}
