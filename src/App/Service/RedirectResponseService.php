<?php

namespace App\Service;

use Application as Container;
use Symfony\Component\HttpFoundation\Request;

class RedirectResponseService
{
    /** @var  Container */
    private $container;
    /** @var  integer */
    private $subRequestType;
    /** @var Request */
    private $request;

    /**
     * @param Container $container
     * @param Request $request
     * @param integer $subRequestType
     */
    public function __construct(
        Container $container,
        Request $request,
        $subRequestType
    ) {
        $this->container = $container;
        $this->request = $request;
        $this->subRequestType = $subRequestType;
    }

    /**
     * @param $url
     * @param int $status
     * @return mixed
     */
    public function redirect($url, $status = 302)
    {
        return $this->container->redirect($url, $status);
    }

    /**
     * @param $url
     * @param string $method
     * @param array $parameters
     * @return mixed
     */
    public function subRequest($url, $method = 'GET', $parameters = array())
    {
        $request = $this->request;
        $subRequest = $request::create($url, $method, $parameters);
        return $this->container->handle($subRequest, $this->subRequestType);
    }
}
