<?php

namespace App\Entity\Auth;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * @ORM\Table(name="users", uniqueConstraints={@UniqueConstraint(name="users_username_uniq", columns={"username"})})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="user_id")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="user_seq", initialValue=1, allocationSize=100)
     */
    private $id;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    public $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    public $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    public $deletedAt;

    /**
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     */
    public $username;

    /**
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     */
    public $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     */
    public $lastName;

    /**
     * @ORM\Column(name="password_crypt", type="string", length=255, nullable=false)
     */
    public $passwordCrypt;

    /**
     * @ORM\Column(name="roles", type="string", length=255, nullable=false)
     */
    public $roles;

    /**
     * Constructor
     */
    public function __construct()
    {
//        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @return integer $id user_id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\PreUpdate
     * @param PreUpdateEventArgs $event
     */
    public function setUpdatedAtTimestamps(PreUpdateEventArgs $event)
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function setTimestampsPrePersist()
    {
        $now = new \Datetime();

        $this->createdAt = $now;
        $this->updatedAt = $now;
    }
    /**
     * @param $createdAt \DateTime
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param $updatedAt \DateTime
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param $deletedAt \DateTime
     */
    public function setDeletedAt(\DateTime $deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }
    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Delete user
     */
    public function delete()
    {
        $this->setDeletedAt(new \DateTime('now'));
    }
}
