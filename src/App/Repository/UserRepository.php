<?php

namespace App\Repository;

use App\Entity\Auth\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

/**
 * Class UserRepository
 * @package App\Repository
 */
class UserRepository extends EntityRepository
{
    /** @var BCryptPasswordEncoder  */
    private $encoder;

    /**
     * UserRepository constructor.
     * @param \Doctrine\ORM\EntityManager $em
     * @param ClassMetadata $class
     * @param BCryptPasswordEncoder $encoder
     */
    public function __construct($em, ClassMetadata $class, BCryptPasswordEncoder $encoder)
    {
        parent::__construct($em, $class);

        $this->encoder = $encoder;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->findBy(array('deletedAt' => null), array('username' => 'ASC'));
    }

    /**
     * @param User $user
     * @param bool $commit
     * @return User
     */
    public function saveUser(User $user, $commit = true)
    {
        $this->getEntityManager()->persist($user);
        if ($commit) {
            $this->getEntityManager()->flush();
        }
        return $user;
    }

    /**
     * @param User $user
     * @param $userData
     * @return User
     */
    public function addUser(User $user, $userData)
    {
        $userCheck = $this->findOneBy(['username'=>$userData['username']]);
        if($userCheck){
            $user = $userCheck;
        }
        return $this->updateUser($user, $userData);
    }

    /**
     * @param User $user
     * @param $userData
     * @return User
     */
    public function updateUser(User $user, $userData)
    {
        $salt = null;

        $user->username  = $userData['username'];
        $user->firstName = $userData['firstName'];
        $user->lastName  = $userData['lastName'];
        $user->roles     = $userData['role'];
        if ($userData['password'] != '' && $userData['password'] == $userData['cPassword']) {
            $user->passwordCrypt = $this->encoder->encodePassword($userData['password'], $salt);
        }

        return $this->saveUser($user);
    }

}
