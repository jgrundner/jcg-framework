<?php
ini_set('display_errors', -1);

// This check prevents access to debug front controllers that are deployed by accident to production servers.
// Feel free to remove this, extend it, or make something more sophisticated.
if (
    isset($_SERVER['HTTP_CLIENT_IP'])
    ||
    isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    ||
    !in_array(@$_SERVER['REMOTE_ADDR'], array('192.168.8.1', '192.168.10.1', '127.0.0.1', 'fe80::1', '::1'))
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.'.$_SERVER['REMOTE_ADDR']);
}

$loader = require_once __DIR__.'/../app/autoload.php';

use Symfony\Component\Debug\Debug;
Debug::enable(E_RECOVERABLE_ERROR & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED, false);

/**
 * Uncomment the following line if your application is hosted behind a reverse
 * proxy at address $ip, and you want Silex to trust the X-Forwarded-For* headers
 */
//Request::setTrustedProxies(array($ip));

$app = new Application();
if ('test' === $app['env']) {
    return $app;
} else {
    $app->run();
}
