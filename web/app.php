<?php
ini_set('display_errors', 0);
$loader = require_once __DIR__.'/../app/autoload.php';

/**
 * Uncomment the following line if your application is hosted behind a reverse
 * proxy at address $ip, and you want Silex to trust the X-Forwarded-For* headers
 */
//Request::setTrustedProxies(array($ip));

$app = new Application();
//dump($app['whoops']);
//$app['whoops'] = $app->extend('whoops', function ($whoops) {
//    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
//    return $whoops;
//});

if ('test' === $app['env']) {
    return $app;
} else {
    $app->run();
}
