<?php

use Symfony\Component\Console\Application;
use Application as App;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\Table;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use App\Command\CacheClearCommand;
use App\Command\LogClearCommand;

/** @var App $app */

$console = new Application('JCG Silex Application', '1.0.0');
$console->getDefinition()->addOption(new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The Environment name.', 'dev'));
$console->setDispatcher($app['dispatcher']);

/*
 * Clear cache directory
 * See: https://github.com/marcojanssen/silex-rest
 */
$command = new CacheClearCommand;
$command->setCachePath($app['cache.path']);
$console->add($command);
/*
 * Clear log files
 */
$command = new LogClearCommand;
$command->setLogPath($app['log.path']);
$console->add($command);

/**
 * Router Debug
 */
$console
    ->register('router:debug')
    ->setDescription('Dumps all the routes with their name, path and requirements')
    ->setCode(function(InputInterface $input, OutputInterface $output) use ($app) {
        $table = new Table($output);
        $table->setHeaders(['Name', 'URI Path', 'Requirements', 'Controller']);

        $controllers    = $app['controllers'];
        $collection     = $controllers->flush();

        foreach ($collection as $name => $route) {
            $requirements = [];
            foreach ($route->getRequirements() as $key => $requirement) {
                $requirements[] = $key . ' => ' . $requirement;
            }

            $table->addRow(array(
                $name,
                $route->getPath(),
                join(', ', $requirements),
                join(', ', $route->getDefaults())
            ));
        }

        $table->render($output);
    });

/*
 * Doctrine CLI
 */
$helperSet = new HelperSet([
    'db' => new ConnectionHelper($app['orm.em']->getConnection()),
    'em' => new EntityManagerHelper($app['orm.em'])
]);
$console->setHelperSet($helperSet);
ConsoleRunner::addCommands($console);


//dump($app['loader']);
//dump($console);
return $console;
