<?php

use App\Loader\RouteLoader;
use Silex\Application as BaseApplication;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Igorw\Silex\ConfigServiceProvider;
use App\Loader\ServiceLoader;
use App\Loader\ServiceProviderLoader;

/**
 * Class Application
 * @package App
 */
class Application extends BaseApplication
{
    /**
     * Application constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this['env'] = getenv('APP_ENV') ?: 'dev';
        $this->register(new ConfigServiceProvider($this->getRootDir()."/app/config/Environments/".$this['env'].".php", [
            'src_path' => $this->getRootDir(),
        ]));

        $this['cache.path'] = $this->getOpenDir() . '/'.$this['cache.dirname'];
        $this['log.path']   = $this->getOpenDir() . '/'.$this['log.dirname'];

        $this->initializeErrorHandling();
        $this->initializeConfiguredServices();
        $this->initializeConfiguredRoutes();

    }

    /**
     * Set up error routing
     */
    private function initializeErrorHandling()
    {
        // Reference alias because we can not use "$this" as lexical variable
        $app =& $this;
        $request = $app['request_stack']->getCurrentRequest();
        $this->error(function (\Exception $e, $code, $app) use ($request) {//dump($e);
            if ($this['debug']) {
                dump($e);
                return null;
            }

            $templates = [
                'errors/'.$code.'.html.twig',
                'errors/'.substr($code, 0, 2).'x.html.twig',
                'errors/'.substr($code, 0, 1).'xx.html.twig',
                'errors/default.html.twig',
            ];

            return new Response($app['twig']->resolveTemplate($templates)->render(['code' => $code]), $code);
        });
    }

    /**
     * Initialize services defined in configuration files
     */
    private function initializeConfiguredServices()
    {
        $serviceProviderLoader = new ServiceProviderLoader();
        $serviceProviderLoader->loadServiceProviders($this);
        $serviceLoader = new ServiceLoader();
        $serviceLoader->loadServices($this);
    }

    /**
     * Initialize routes defined in configuration files
     */
    private function initializeConfiguredRoutes()
    {
        $routeLoader = new RouteLoader();
        $routeLoader->loadRoutes($this);
    }

    /**
     * Get root directory.
     * @return string|string
     */
    public function getRootDir()
    {
        static $dir;
        if (empty($dir)) {
            $rc = new \ReflectionClass(get_class($this));
            $dir = dirname(dirname($rc->getFileName()));
        }
        return $dir;
    }

    /**
     * Get writable directory.
     *
     * @return string
     */
    public function getOpenDir()
    {
        static $dir;
        if (empty($dir)) {
            $dir = $this->getRootDir() . '/'.$this['write.dirname'];
        }
        return $dir;
    }

    /**
     * Get cache directory.
     *
     * @return string
     */
    public function getCacheDir()
    {
        static $dir;
        if (empty($dir)) {
            $dir = $this['cache.path'];

            if (!is_dir($dir)) {
                mkdir($dir, 0755, true);
            }
        }
        return $dir;
    }

    /**
     * Get log directory.
     *
     * @return string
     */
    public function getLogDir()
    {
        static $dir;
        if (empty($dir)) {
            $dir = $this['log.path'];

            if (!is_dir($dir)) {
                mkdir($dir, 0755, true);
            }
        }
        return $dir;
    }

    /**
     * Pass Through Function
     * This is to deal with third party code that still relies on Silex 1 sharing
     *
     * @param $callable
     * @return mixed
     */
    public function share($callable)
    {
        return $callable;
    }

}
