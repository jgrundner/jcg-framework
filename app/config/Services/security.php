<?php
/** @var Application $app */

use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

/**
 * @return BCryptPasswordEncoder
 */
$app['security.encoder.digest'] = function () {
    return new BCryptPasswordEncoder(4);
};

return $app;
