<?php
/** @var Application $app */

use App\Service\RedirectResponseService;

/**
 * @param Application $app
 * @return RedirectResponseService
 */
$app['redirect_response.service'] = function ($app) {
    return new RedirectResponseService(
        $app,
        $app['request_stack']->getCurrentRequest(),
        Symfony\Component\HttpKernel\HttpKernelInterface::SUB_REQUEST
    );
};
