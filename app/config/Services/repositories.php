<?php
/** @var Application $app */

/**
 * @param Application $app
 * @return App\Repository\UserRepository
 */
$app['user.repository'] = function($app) {
    return new App\Repository\UserRepository(
        $app['orm.em'],
        new Doctrine\ORM\Mapping\ClassMetadata('App\Entity\Auth\User'),
        $app['security.encoder.digest']
    );
};

return $app;
