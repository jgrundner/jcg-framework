<?php
/** @var Application $app */

use App\Controller\PublicController;
use App\Controller\DashboardController;
use App\Controller\UserController;

/**
 * @param Application $app
 * @return PublicController
 */
$app['publicApp.controller'] = function ($app) {
    return new PublicController($app['twig']);
};
/**
 * @param Application $app
 * @return DashboardController
 */
$app['dashboard.controller'] = function ($app) {
    return new DashboardController($app['twig']);
};

/**
 * @param Application $app
 * @return UserController
 */
$app['user.controller'] = function ($app) {
    return new UserController(
        $app['twig'],
        $app['url_generator'],
        $app['redirect_response.service'],
        $app['user.repository']
    );
};

return $app;
