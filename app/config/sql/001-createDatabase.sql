/*******************************************************************************
* 001-createDatabase
*
* Description: Initial user setup
*
* Created On: 9/6/15
* Revised On: 10/2/15
* Author: jgrundner
******************************************************************************/

--DROP TABLE users;
 CREATE TABLE users (
   user_id        INTEGER      NOT NULL,
   created_at     DATETIME     NOT NULL,
   updated_at     DATETIME DEFAULT NULL,
   deleted_at     DATETIME DEFAULT NULL,
   username       VARCHAR(225) NOT NULL,
   first_name     VARCHAR(255) NOT NULL,
   last_name      VARCHAR(255) NOT NULL,
   password_crypt VARCHAR(255) NOT NULL,
   roles          VARCHAR(255) NOT NULL,
   PRIMARY KEY (user_id)
 );
 CREATE UNIQUE INDEX users_username_uniq ON users (username);



INSERT INTO users (created_at, updated_at, deleted_at, username, first_name, last_name, password_crypt, roles)
VALUES
    /* BCrypt Password: $2y$04$bZuC8L4I4IkFRIUoZcYUHO0QbQYYun8/1JWontvwoBM1ULWjJbksy = foo */
    (CURRENT_TIMESTAMP, NULL, NULL, 'admin@jgrundner.com', 'Admin', 'Admin', '$2y$04$bZuC8L4I4IkFRIUoZcYUHO0QbQYYun8/1JWontvwoBM1ULWjJbksy', 'ROLE_ADMIN'),
    (CURRENT_TIMESTAMP, NULL, NULL, 'jimigrunge@yahoo.com', 'Jim', 'Grundner', '$2y$04$bZuC8L4I4IkFRIUoZcYUHO0QbQYYun8/1JWontvwoBM1ULWjJbksy', 'ROLE_ADMIN'),
    (CURRENT_TIMESTAMP, NULL, NULL, 'DoctorSmith@jgrundner.com', 'John A', 'Smith', '$2y$04$bZuC8L4I4IkFRIUoZcYUHO0QbQYYun8/1JWontvwoBM1ULWjJbksy', 'ROLE_MANAGER'),
    (CURRENT_TIMESTAMP, NULL, NULL, 'TheMaster@jgrundner.com', 'Miss', 'Tress', '$2y$04$bZuC8L4I4IkFRIUoZcYUHO0QbQYYun8/1JWontvwoBM1ULWjJbksy', 'ROLE_DEVELOPER'),
    (CURRENT_TIMESTAMP, NULL, NULL, 'one@jgrundner.com', 'one1', '1one', '$2y$04$bZuC8L4I4IkFRIUoZcYUHO0QbQYYun8/1JWontvwoBM1ULWjJbksy', 'ROLE_DEVELOPER');


