<?php
/** @var Application $app */

// Routing
$app->match('/login',               "publicApp.controller:loginAction")->bind('login');
$app->match('/',                    "publicApp.controller:indexAction")->bind('homepage');
$app->get  ('/dashboard',           "dashboard.controller:indexAction")->bind('dashboard');
$app->get  ('/user',                "user.controller:indexAction")     ->bind('users');
$app->get  ('/user/edit/{userId}',  "user.controller:editAction")      ->bind('user_edit');
$app->post ('/user/save/{userId}',  "user.controller:saveAction")      ->bind('user_save');
$app->get  ('/user/add',            "user.controller:addAction")       ->bind('user_add');
$app->post ('/user/create',         "user.controller:createAction")    ->bind('user_create');
$app->get  ('/user/delete/{userId}',"user.controller:deleteAction")    ->bind('user_delete');

return $app;
