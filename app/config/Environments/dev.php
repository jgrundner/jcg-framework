<?php
return [
    "app.name" => "JCG Framework",
    "debug" => true,
    "log" => [
        "level" => "DEBUG"
    ],
    "twig.options" => "%src_path%/var/cache/twig",
    "twig.path" => "%src_path%/templates",
    "application.root" => "%src_path%",
    "write.dirname" => "var",
    "cache.dirname" => "cache",
    "log.dirname" => "logs",
    "config.dirname" => "config",
    "services.dirname" => "%src_path%/app/config/Services",
    "routers.dirname" => "%src_path%/app/config/Routes",
    "monolog.logfile" => "",
    "profiler.cache_dir" => "%src_path%/var/cache/profiler",
    "annotation.cache_dir" => "%src_path%/var/cache/annotations",
    "firewall" => [
        "login.pattern" => "^/login$",
        "login.anonymous" => true,
        "admin.pattern" => "^/(admin|user|dashboard)*",
        "admin.http" => true,
        "admin.anonymous" => true,
        "admin.form" => [
            "login_path" => "/login",
            "check_path" => "/admin/login_check",
            "default_target_path" => "dashboard"
        ],
        "admin.logout_path" => "/admin/logout",
        "anon.pattern" => "^/",
        "anon.anonymous" => true
    ],
    "db.options" => [
        "driver" => "pdo_sqlite",
        "path" => "%src_path%/app.db",
        "host" => "",
        "port" => "",
        "dbname" => "",
        "user" => "",
        "password" => "",
        "charset" => ""
    ],
    "orm.proxies_dir" => "%src_path%/var/cache/doctrine/proxy",
    "orm.em.options" => [
        "mappings" => [
            [
                "type" => "annotation",
                "namespace" => "App\\Entity",
                "path" => "%src_path%/src/App/Entity",
                "resources_namespace" => "App\\Entity",
                "use_simple_annotation_reader" => false
            ]
        ]
    ]
];
