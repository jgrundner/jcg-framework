#!/bin/sh
# Add any extra stuff you might need here.

sudo rm /etc/nginx/conf.d/default.conf
# Create httpd conf for default mapped directory
if [ ! -e /etc/nginx/conf.d/default.conf ]; then
    sudo touch /etc/nginx/conf.d/default.conf
    sudo tee /etc/nginx/conf.d/default.conf <<'EOF'
server {
    root /var/www/project/web;

    location / {
        try_files $uri @rewriteapp;
    }

    location @rewriteapp {
        rewrite ^(.*)$ /app.php/$1 last;
    }

    location ~ ^/(config|app|app_dev)\.php(/|$) {
        fastcgi_pass            unix:/var/run/php-fpm.sock;
        fastcgi_intercept_errors        on;

        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
    error_log   /var/log/nginx/error.log;
    access_log  /var/log/nginx/access.log;

}
EOF
fi

# Update Apt Get
sudo apt-get update

# Install utilities
sudo apt-get install htop

# Add PostgreSQL to machine (Looks like it may already be installed)
#sudo apt-get install --yes postgresql-9.4 postgresql-client-9.4

# ------------------------------------------------
# Install and configure mailcatcher as a service
# ------------------------------------------------
# Make sure we have dependancies
sudo apt-get install --yes libsqlite3-dev
sudo apt-get install --yes ruby-dev
if ! gem -v > /dev/null 2>&1; then
  echo "aptitude install -y libgemplugin-ruby";
  sudo aptitude install -y libgemplugin-ruby;
fi

# Install mailcatcher
sudo gem install --no-rdoc --no-ri mailcatcher
if [ ! -e /etc/systemd/system/mailcatcher.service ]; then
    sudo touch /etc/systemd/system/mailcatcher.service
    sudo tee /etc/systemd/system/mailcatcher.service <<EOL
    [Unit]
    Description=Ruby MailCatcher
    Documentation=http://mailcatcher.me/

    [Service]
    # Ubuntu/Debian convention:
    EnvironmentFile=-/etc/default/mailcatcher
    Type=simple
    ExecStart=$(which mailcatcher) --foreground --http-ip 0.0.0.0

    [Install]
    WantedBy=multi-user.target
EOL
fi
systemctl enable mailcatcher.service
systemctl start mailcatcher.service

# Make php use it to send mail
if [ ! -e /etc/php7/mods-available/mailcatcher.ini ]; then
    if [ ! -e /etc/php7/mods-available ]; then
        sudo mkdir /etc/php7/mods-available
    fi
    sudo touch /etc/php7/mods-available/mailcatcher.ini
    echo "sendmail_path = /usr/bin/env $(which catchmail)" | sudo tee /etc/php7/mods-available/mailcatcher.ini
    sudo mkdir -p /etc/php7/fpm && sudo ln -s /etc/php7/mods-available /etc/php7/fpm/conf.d
fi
# ------------------------------------------------

# ------------------------------------------------
# Install the symfony installer
# ------------------------------------------------
#if [ ! -e /usr/local/bin/symfony ]; then
#    sudo curl -LsS http://symfony.com/installer -o /usr/local/bin/symfony
#    sudo chmod a+x /usr/local/bin/symfony
#fi
# ------------------------------------------------

# Stop the MySQL server service for now as it is eating 40+% for memory
sudo /etc/init.d/mysql stop

# Restart web services to make sure everything is loaded
sudo service php-fpm restart
sudo service nginx restart



