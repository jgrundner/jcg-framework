#!/usr/bin/env bash

echo ">>> Installing Symfony"

# Test if PHP is installed
php -v > /dev/null 2>&1 || { printf "!!! PHP is not installed.\n    Installing Symfony aborted!\n"; exit 0; }
# Test if Composer is installed
composer -v > /dev/null 2>&1 || { printf "!!! Composer is not installed.\n    Installing Symfony aborted!\n"; exit 0; }
# Test if Symfony installer is installed
symfony -v > /dev/null 2>&1 || { printf "!!! Symfony is not installed.\n    Installing Symfony aborted!\n"; exit 0; }
# Test if Server IP is set in Vagrantfile
[[ -z "$1" ]] && { printf "!!! IP address not set. Check the Vagrantfile.\n    Installing Symfony aborted!\n"; exit 0; }

# -- Setup variables
param_host_id="$1"
public_folder="$2"
symfony_folder="$3"
symfony_project="$4"
symfony_root_folder="${public_folder}/${symfony_folder}"
symfony_project_folder="${symfony_root_folder}/${symfony_project}"
#symfony_project_folder="${public_folder}/${symfony_folder}"
symfony_public_folder="${symfony_project_folder}/web"
#symfony_public_folder="${symfony_project_folder}/web"

# The host ip is same as guest ip with last octet equal to 1
host_ip=`echo $1 | sed 's/\.[0-9]*$/.1/'`

# Test if Apache or Nginx is installed
nginx -v > /dev/null 2>&1
NGINX_IS_INSTALLED=$?

apache2 -v > /dev/null 2>&1
APACHE_IS_INSTALLED=$?

# Create Symfony folder if needed
if [ ! -d ${symfony_root_folder} ]; then
    mkdir -p ${symfony_root_folder}
fi

# We do not have a pre configured composer.json
if [ ! -f "${symfony_project_folder}/composer.json" ]; then
    echo "Using symfony.phar"
    echo ">> cd $symfony_root_folder"
    cd ${symfony_root_folder}

    echo ">> symfony new $symfony_project"
    symfony new ${symfony_project}
# We do
else
    echo "Using composer.phar"
    echo ">> cd ${symfony_project_folder}"
    cd ${symfony_project_folder}

    echo ">> composer install --prefer-dist"
    composer install --prefer-dist

    # Go to the previous folder
    cd -
fi

sudo chmod -R 775 ${symfony_project_folder}/app/cache
sudo chmod -R 775 ${symfony_project_folder}/app/logs
sudo chmod -R 775 ${symfony_project_folder}/app/console
cd ${public_folder}
sudo chown -R www-data ${symfony_folder}

sed -i "s/('127.0.0.1', 'fe80::1'/('127.0.0.1', '$host_ip', 'fe80::1'/" ${symfony_public_folder}/app_dev.php
sed -i "s/'127.0.0.1',$/'127.0.0.1', '$host_ip',/" ${symfony_public_folder}/config.php

if [ ${NGINX_IS_INSTALLED} -eq 0 ]; then
    # Change default vhost created
    sudo sed -i "s@root /vagrant@root ${symfony_public_folder}@" /etc/nginx/sites-available/vagrant
    sudo service nginx reload
fi

if [ ${APACHE_IS_INSTALLED} -eq 0 ]; then
    # Find and replace to find public_folder and replace with laravel_public_folder
    # Change DocumentRoot
    # Change ProxyPassMatch fcgi path
    # Change <Directory ...> path
    sudo sed -i "s@$public_folder@$symfony_public_folder@" /etc/apache2/sites-available/$1.xip.io.conf

    sudo service apache2 reload
fi

# We do not have a pre configured composer.json
#if [ ! -f "${symfony_project_folder}/composer.json" ]; then
#    echo "Using symfony.phar"
#    echo ">> cd ${public_folder}"
#    cd ${public_folder}
#
#    echo ">> symfony new ${symfony_folder}"
#    symfony new ${symfony_folder}
## We do
#else
#    echo "Using composer.phar"
#    echo ">> cd ${symfony_project_folder}"
#    cd ${symfony_project_folder}
#
#    echo ">> composer install --prefer-dist"
#    composer install --prefer-dist
#
#    # Go to the previous folder
#    cd -
#fi
#
#sudo chmod -R 775 ${symfony_project_folder}/app/cache
#sudo chmod -R 775 ${symfony_project_folder}/app/logs
#sudo chmod -R 775 ${symfony_project_folder}/app/console
#
#sed -i "s/('127.0.0.1', 'fe80::1'/('127.0.0.1', '$host_ip', 'fe80::1'/" ${symfony_public_folder}/app_dev.php
#sed -i "s/'127.0.0.1',$/'127.0.0.1', '$host_ip',/" ${symfony_public_folder}/config.php
#
#if [ ${NGINX_IS_INSTALLED} -eq 0 ]; then
#    # Change default vhost created
#    sudo sed -i "s@root /vagrant@root ${symfony_public_folder}@" /etc/nginx/sites-available/vagrant
#    sudo service nginx reload
#fi
#
#if [ ${APACHE_IS_INSTALLED} -eq 0 ]; then
#    # Find and replace to find public_folder and replace with laravel_public_folder
#    # Change DocumentRoot
#    # Change ProxyPassMatch fcgi path
#    # Change <Directory ...> path
#    sudo sed -i "s@$public_folder@$symfony_public_folder@" /etc/apache2/sites-available/$1.xip.io.conf
#
#    sudo service apache2 reload
#fi
