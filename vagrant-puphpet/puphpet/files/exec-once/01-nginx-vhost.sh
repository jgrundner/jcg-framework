#!/bin/sh
# Add any extra stuff you might need here.

sudo rm /etc/nginx/sites-enabled/*

if [ ! -e /etc/nginx/sites-available/vhost.conf ]; then
    sudo touch /etc/nginx/sites-available/vhost.conf
    sudo tee /etc/nginx/sites-available/vhost.conf <<'EOF'
server {
    root /var/www/project/web;

    location / {
        try_files $uri @rewriteapp;
    }

    location @rewriteapp {
        rewrite ^(.*)$ /app.php/$1 last;
    }

    location ~ ^/(config|app|app_dev)\.php(/|$) {
        fastcgi_pass               127.0.0.1:9000;
        fastcgi_intercept_errors   on;

        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include /etc/nginx/fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param APP_ENV dev;
    }
    error_log   /var/log/nginx/error.log;
    access_log  /var/log/nginx/access.log;

}
EOF
fi

sudo ln -s /etc/nginx/sites-available/vhost.conf /etc/nginx/sites-enabled/vhost.conf
sudo service php7-fpm restart
sudo service nginx restart

